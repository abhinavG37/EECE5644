function [sigma] = regularizedCov(X)
%Calculate the Regularized covariance matrix
n = size(X ,2);
sigma = cov(X);

alpha = 0.5;
lambda_reg = alpha * (trace(sigma) / rank(sigma));
lambda_reg = 1/n;
sigma = sigma+ lambda_reg * eye(n);
end

